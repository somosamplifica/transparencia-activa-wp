## Instalación

** Base de datos **

En el root se encuentra la base de datos en formato .sql

** Bedrock **

```shell
# @ /
$ composer install
```

** Sage **

```shell
# @ /web/app/themes/theme-name/
$ composer install
$ yarn install
```

** Compilación de recursos **

* `yarn start` — Compile assets when file changes are made, start Browsersync session
* `yarn build` — Compile and optimize the files in your assets directory
* `yarn build:production` — Compile assets for production

### Deploy en Jusbaires

** Instancia Jusbaires**

** Permisos **

* `sudo /bin/chown -R usuario.www-data /var/www/beta-transparencia.juscaba.gob.ar`
* `sudo /bin/chown -R www-data.www-data /var/www/beta-transparencia.juscaba.gob.ar`

** Proxy **

Composer

* `export HTTP_PROXY="central2-iws1.jusbaires.gob.ar:8080"`
* `export HTTPS_PROXY="central2-iws1.jusbaires.gob.ar:8080"`

GIT

* `git config --global http.proxy central2-iws1.jusbaires.gob.ar:8080`
* `git config --global https.proxy central2-iws1.jusbaires.gob.ar:8080`

** Log de Errores **

* `sudo /usr/bin/tail -f /var/log/apache2/beta-transparencia.juscaba.gob.ar_error.log`
* `sudo /usr/bin/tail -f /var/log/apache2/transparencia.juscaba.gob.ar_error.log`