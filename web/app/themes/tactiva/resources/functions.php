<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);
    

add_action("wp_enqueue_scripts", "dcms_insertar_google_fonts");

function dcms_insertar_google_fonts(){
    $url = "https://fonts.googleapis.com/css2?family=Sarabun:wght@400;600;800&display=swap";
    wp_enqueue_style('google_fonts', $url);
 }

/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

// Agregar bloques de Gutemberg
function my_acf_block_render_callback( $block ) { 
    $slug = str_replace('acf/', '', $block['name']);
    $block['slug'] = $slug;
    if(empty($block['className'])){ $block['className'] = ''; }
    $block['classes'] = implode(' ', [$block['slug'], $block['className'], $block['align']]);
    echo \App\template("blocks/${slug}", ['block' => $block]);
}


class tactiva_widget extends WP_Widget {
    function __construct() {
        parent::__construct(
        // widget ID
        'tactiva_widget',
        // widget name
        __('Acceso a la información', 'tactiva_widget_domain'),
        // widget description
        array ( 'description' => __( 'Acceso a la información Transparencia Activa', 'tactiva_widget_domain' ), )
        );
    }
    public function widget( $args, $instance ) {
        $title = $instance['title']; 
        $link = apply_filters( 'widget_link', $instance['link'] ); 
        //if title is present
        if ( ! empty ( $title ) ) 
        ?> 
        <section class="ley-104">
            <div class="container">
                <div class="row row d-flex justify-content-center">
                    <div class="col-lg-8 col-12">
                        <?php if ( ! empty ( $title ) ){ ?>
                        <p class="ley-104__texto"><?php echo $title ?></p>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4 col-12">
                        <?php if ( ! empty ( $link ) ){ ?>
                        <a href="<?php echo $link ?>" class="btn btn-primary btn-descargar">Iniciar Trámite</a>
                        <?php } ?>
                    </div> 
                </div>
            </div>
        </section>
        <?php 
    }
    public function form( $instance ) {

        if ( isset( $instance[ 'title' ] ) )
            $title = $instance[ 'title' ];
        else 
            $title = __( 'Texto', 'tactiva_widget_domain' );
        ?> 
        <p>
        <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Texto:' ); ?></label>
        <textarea class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text"><?php echo esc_attr( $title ); ?></textarea>
        </p> 
        <?php
        if ( isset( $instance[ 'link' ] ) )
            $link = $instance[ 'link' ];
        else 
            $link = __( 'Link', 'tactiva_widget_domain' );
        ?> 
        <p>
        <label for="<?php echo $this->get_field_id( 'link' ); ?>"><?php _e( 'Link:' ); ?></label>
        <input class="widefat" id="<?php echo $this->get_field_id( 'link' ); ?>" name="<?php echo $this->get_field_name( 'link' ); ?>" type="text" value="<?php echo esc_attr( $link ); ?>" />
        </p> <?php 
    }
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? $new_instance['title'] : '';
        $instance['link'] = ( ! empty( $new_instance['link'] ) ) ? strip_tags( $new_instance['link'] ) : ''; 
        return $instance;
    }
}

function tactiva_register_widget() { 
    register_widget( 'tactiva_widget' );
}
add_action( 'widgets_init', 'tactiva_register_widget' );

 

add_theme_support('soil', [
    'clean-up',
    'disable-asset-versioning',
    'disable-trackbacks', 
    'js-to-footer',
    'nav-walker',
    'nice-search',
    'relative-urls'
]);


function add_google_tag_manager_head() { ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-4NR0ELCZZ3"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-4NR0ELCZZ3');
    </script>

<?php }
add_action('wp_head', 'add_google_tag_manager_head');

