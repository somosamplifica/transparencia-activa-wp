{{--
  Template Name: Hero
--}}

@extends('layouts.app')

@section('content')
    <div class="migas-de-pan migas-de-pan--formulario">
        <div class="container">
        @php 
        do_action('migas_de_pan');
        @endphp
    </div>
    </div>
        <!--   Hero -->
    <section class="hero hero--form">
        <div class="hero__img">{!! get_the_post_thumbnail() !!}</div>
        <div class="container">
            <div class="row d-flex justify-content-center align-items-center">
                <h1 class="hero__titulo">{!! get_the_title() !!}</h1>
                <p class="hero__texto">{!! get_the_excerpt() !!}</p>
            </div>
        </div>
    </section>
    <!-- Cuerpo -->
    <section class="contenido-form">
        <div class="container">
            <div class="row">
                <div class="texto-form"> 
                    {!! the_content() !!}
                </div> 
            </div>
        </div>
    </section>
@endsection
