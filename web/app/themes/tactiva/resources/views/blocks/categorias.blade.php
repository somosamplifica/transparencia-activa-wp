{{--
	Title: Categorías Transparencia Activa
	Description: Categorías 
	Category: formatting
	Icon: admin-comments
	Keywords: hero
	Mode: edit
	Align: left
	PostTypes: page post
	SupportsAlign: left right
	SupportsMode: false
	SupportsMultiple: false
--}} 
<!-- Sección Categorías -->
<section class="categorias" data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    <div class="container ">
        <div class="row d-flex justify-content-center">
            <h2 class="categorias__titulo">{{ get_field('titulo') }}</h2>
        	<div class="contenidoModulo">{!! get_field('descripcion') !!}</div>
        </div>
        <div class="row row d-flex justify-content-center">  
            @if(FrontPage::categoriasLoop())
              @foreach(FrontPage::categoriasLoop() as $term)  
                <div class="col-lg-3 col-12">
                    <a href="?s=&_categorias={{ $term->slug }}" class="categorias__enlace"> 
                        <div class="categorias_enlace_icon">{!! get_field('icono', $term) !!}</div>
                        <span class="categorias__texto">{{ $term->name }}</span>
                    </a>
                </div> 
              @endforeach
            @endif 
        </div>
    </div>
</section>