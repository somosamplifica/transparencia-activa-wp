{{--
	Title: Organismos Transparencia Activa
	Description: organismos 
	Category: formatting
	Icon: admin-comments
	Keywords: hero
	Mode: edit
	Align: left
	PostTypes: page post
	SupportsAlign: left right
	SupportsMode: false
	SupportsMultiple: false
--}}

<!-- Accesos directos por organismo -->
<section class="accesos-directos" data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    <div class="container">
        <h2 class="accesosdirectos__title">{{ get_field('titulo') }}</h2>
        <div class="contenidoModulo">{!! get_field('descripcion') !!}</div>
        <div class="row row row d-flex justify-content-center"> 
            @if(FrontPage::organismosLoop()) 
              @foreach(FrontPage::organismosLoop() as $term)   
                <div class="col-lg-4 col-12 bloques-organismos"> 
                    <a href="?s=&_organismos={{ $term->slug }}" class="accesos-directos__img">
                        {!! wp_get_attachment_image(get_field('foto', $term), 'homepage-thumb' ); !!}
                        <h3 class="{{ $term->slug }}_texto">{{ $term->name }}</h3>
                    </a>
                </div> 
              @endforeach
            @endif 
        </div> 
    </div>
</section>