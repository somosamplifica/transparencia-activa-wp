@extends('layouts.app')

@section('content')
  @if (!have_posts())
    <section class="hero_404">
      <div class="container"> 
        <h1 class="hero__titulo">404</h1>
        <p class="hero__texto">Contenido no encontrado.</p> 
          <form role="search" method="get" class="input-group mb-3 hero__buscador" action="{{ home_url('/') }}">
              <input type="text" class="form-control" placeholder="Ingresá un término de búsqueda: Sentencias, Presupuesto..." name="s" >
              <div class="input-group-append">
                  <button class="btn btn-outline-secondary" type="submit" id="button-addon2"><i class="fa fa-search" aria-hidden="true"></i></button>
              </div>
          </form>  
      </div>
    </section> 
  @endif
  @php 
     dynamic_sidebar( 'sidebar-top-footer' );  
  @endphp
@endsection
