@extends('layouts.app')

@section('content')
	@while(have_posts()) @php the_post() @endphp 
		@include('partials.content-page')
	@endwhile
	@php 
	   dynamic_sidebar( 'sidebar-top-footer' );  
	@endphp
@endsection
