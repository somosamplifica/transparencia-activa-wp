<article @php post_class() @endphp>
  <section class="contenido-estatico">
        <div class="container">
            <div class="row">
                <h3 class="titulo-estatico">{!! get_the_title() !!}</h3>
                <p class="texto-estatico">@php the_content() @endphp</p>  
            </div>
        </div>
    </section>
</article>
