<article @php post_class() @endphp>
    <div class="migas-de-pan">
        <div class="container">
        @php 
        do_action('migas_de_pan');
        @endphp
    </div>
    </div> 
    <!-- Cuerpo -->
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-12">
                <section id="interna-contenido">
                    <h3 class="titulo__contenido">
                        {!! get_the_title() !!}
                    </h3> 
                    <div class="texto__contenido">
                        {!! the_content() !!}
                    </div> 
                    <div class="acceder-informacion">
                        <h4 class="titulo__acceso">Recursos del dataset</h4> 

                      @if( get_field('recursos') )
                      <ul class="list_recursos">
                          @while( the_repeater_field('recursos'))  
                                @php 
                                  $id = get_sub_field('formato');  
                                  $term = get_term_by('term_id', $id, 'formato'); 
                                  $format = get_field('icono', $term);   
                                @endphp  
                                <li><span class="icon-format"><a href="{{the_sub_field('link')}}" target="_blank">{!! $format !!}</a></span><a href="{{the_sub_field('link')}}" target="_blank">{{the_sub_field('nombre_del_recurso')}}</a><a href="{{the_sub_field('link')}}" class="btn btn-primary btnTransparencia--contenido" target="_blank">Consultar</a></li>
                          @endwhile
                      </ul>
                      @endif
                         
                    </div>
                </section>
            </div>
            <div class="col-lg-4 col-12">
                <section id="sidebar-contenido">
                    <h3 class="titulo__sidebar-contenido">Acerca de este dataset</h3>
                        <ul class="lista__organismos">
                            @php
                              $organismo_loop = wp_get_post_terms( $post->ID, 'organismo');   
                              if($organismo_loop){
                                foreach($organismo_loop as $organismo){   
                                  echo '<li class="elemento__organismos"><a href="'.home_url('/').'?s=&_organismos='.$organismo->slug.'"><span class="item__organismos item_'.$organismo->slug.'">'.$organismo->name.'</span></a></li>';  
                                }
                              } 
                            @endphp   
                        </ul>

                    <ul class="lista__categorias">
                        @php
                              $categorias_loop = wp_get_post_terms( $post->ID, 'categoria');   
                              if($categorias_loop){
                                foreach($categorias_loop as $categoria){    
                                  echo '<li class="elemento__categorias"><span class="icono_categoria">'.get_field('icono', $categoria).'</span><span class="item__categorias">'.$categoria->name.'</span></li>'; 
                                }
                              } 
                            @endphp 
                      
                    </ul>   
                    @empty(!get_field('frecuencia_de_actualizacion'))
                    <div class="actualizacion"><span class="reloj-actualizacion">Frecuencia de actualización</div>
                    <div class="actualizacion__valor"><i class="fas fa-clock"></i></span> {!! get_field("frecuencia_de_actualizacion") !!}</div>
                    @endempty
                </section>
            </div>
        </div> 
    </div> 
</article>