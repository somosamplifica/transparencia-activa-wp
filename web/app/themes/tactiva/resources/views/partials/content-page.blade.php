<article @php post_class() @endphp>
    <div class="migas-de-pan migas-de-pan--formulario">
        <div class="container">
        @php 
        do_action('migas_de_pan');
        @endphp
    </div>
    </div>
   <section class="contenido-estatico">
        <div class="container">
            <div class="row">
                <h3 class="titulo-estatico">{!! get_the_title() !!}</h3>
                <p class="texto-estatico">@php the_content() @endphp</p>  
            </div>
        </div>
    </section>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
  </footer>
{{--   @php comments_template('/partials/comments.blade.php') @endphp --}}
</article>

   
   