<footer>
    <section class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-12 footer-aligment"> 
                  @php 
                     dynamic_sidebar( 'sidebar-footer-1' );  
                  @endphp 
                </div>
                <div class="col-lg-4 col-12 footer-aligment">
                  @php 
                     dynamic_sidebar( 'sidebar-footer-2' );  
                  @endphp 
                </div>
                <!-- <div class="col-lg-3 col-12">
                  @php 
                     dynamic_sidebar( 'sidebar-footer-3' );  
                  @endphp
                </div> -->
                <div class="col-lg-4 col-12 footer-aligment">
                  @php 
                     dynamic_sidebar( 'sidebar-footer-4' );  
                  @endphp 
                </div>
            </div>
        </div>
    </section>
    <section class="footer-bottom">
        <div class="container">
            <div class="row ">
              @php 
                 dynamic_sidebar( 'sidebar-bottom-footer' );  
              @endphp 
            </div>
        </div>
    </section>
</footer>